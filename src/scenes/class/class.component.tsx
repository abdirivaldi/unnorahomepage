type Props = {
  name: string;
  description?: string;
  image: string;
};

const Class = ({ name, description, image }: Props) => {
  const overlayStyles = `p-5 absolute z-30 flex
  h-[380px] w-[450px] flex-col items-center justify-center
  whitespace-normal bg-primary-500 text-center text-white
  opacity-0 transition duration-500 hover:opacity-90`;

  const imageStyles = `w-full h-full object-cover object-center`; // Style tambahan untuk <img>

  return (
    <li className="relative mx-5 inline-block h-[380px] w-[450px] overflow-hidden"> {/* Tambahkan overflow-hidden jika perlu */}
      <div className={overlayStyles}>
        <p className="text-2xl">{name}</p>
        <p className="mt-5">{description}</p>
      </div>
      <img alt={name} src={image} className={imageStyles} /> {/* Tambahkan className ke <img> */}
    </li>
  );
};

export default Class;