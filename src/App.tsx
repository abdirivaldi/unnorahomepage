import { useEffect, useState } from "react"
import Home from "./scenes/home/home.component"
import Navbar from "./scenes/navbar/navbar.component"
import { SelectedPage } from "./shared/types"
import Benefits from "./scenes/benefit/benefit.service"
import ContactUs from "./scenes/contactUs/contactUs.component"
import Collection from "./scenes/class/class.service"


function App() {
  const [selectedPage,setSelectedPage] = useState<SelectedPage> (SelectedPage.Home)
  const [isTopOfPage, setIsTopOfPage] = useState<boolean>(true);

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY === 0) {
        setIsTopOfPage(true);
        setSelectedPage(SelectedPage.Home);
      }
      if (window.scrollY !== 0) setIsTopOfPage(false);
    };
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  return (
<div className='app bg-gray-20'>
<Navbar
        isTopOfPage={isTopOfPage}
        selectedPage={selectedPage}
        setSelectedPage={setSelectedPage}
      />
      <Home setSelectedPage={setSelectedPage} />
      <Benefits setSelectedPage={setSelectedPage}/>
      <Collection setSelectedPage={setSelectedPage}/>
      <ContactUs setSelectedPage={setSelectedPage}/>
</div>
  )
}

export default App
